export const getEmployeeList = (req: any, res: any, client: any) => {
    client.query(`SELECT * FROM "employee"`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'getEmployeeList')
        } else {
            res.json(result.rows)
        }
    })
}

export const getUserInfo = (req: any, res: any, client: any) => {
    const { empId } = req.query
    client.query(`SELECT * FROM "employee" WHERE "EMP_ID" = '${empId}'`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'getEmployeeList')
        } else {
            res.json(result.rows)
        }
    })
}
export const getUserAccessibility = (req: any, res: any, client: any) => {
    const { empId } = req.query
}

export const getGroupList = (req: any, res: any, client: any) => {
    client.query(`SELECT * FROM "group"`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'getGroupList')
        } else {
            res.json(result.rows)
        }
    })
}

export const getWorkDayList = (req: any, res: any, client: any) => {
    client.query(`SELECT * FROM "work_and_ot_record" WHERE "TYPE" = 'Work Day'`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'getWorkDayList')
        } else {
            res.json(result.rows)
        }
    })
}

export const getOtList = (req: any, res: any, client: any) => {
    const { startDate, endDate } = req.query
    client.query(`SELECT w."SK", w."DATE", w."START_TIME", w."END_TIME", w."HOURS", w."TYPE", w."STATUS", e."FIRST_NAME_TH", e."LAST_NAME_TH" FROM work_and_ot_record as w
    INNER JOIN employee as e on w."EMP_ID" = e."EMP_ID" WHERE "TYPE" = 'OT' AND "DATE" BETWEEN '${startDate}' AND '${endDate}'`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'getOtList')
        } else {
            res.json(result.rows)
        }
    })
}

export const get_ot_leave_dayoff_List = (req: any, res: any, client: any) => {
    const { startDate, endDate } = req.query
    console.log(startDate, endDate);
    client.query(`SELECT w."SK", w."EMP_ID", w."DATE", w."START_TIME", w."END_TIME", w."HOURS", w."TYPE", w."STATUS", e."FIRST_NAME_TH",e."LAST_NAME_TH"  FROM work_and_ot_record as w
    JOIN employee as e ON w."EMP_ID" = e."EMP_ID" WHERE w."DATE" BETWEEN '${startDate}' AND '${endDate}'
    UNION
    SELECT l."SK", l."EMP_ID", l."LEAVE_DATE", NULL, NULL, NULL, l."LEAVE_TYPE", l."STATUS", e."FIRST_NAME_TH", e."LAST_NAME_TH" FROM leave_record as l
    JOIN employee as e ON l."EMP_ID" = e."EMP_ID" WHERE l."LEAVE_DATE" BETWEEN '${startDate}' AND '${endDate}'`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'get_ot_leave_dayoff_List')
        } else {
            res.json(result.rows)
        }
    })
}