import express, { Request, Response } from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import { Client } from 'pg'
import cors from 'cors'

import { getEmployeeList, getUserInfo, getUserAccessibility, getGroupList, getWorkDayList, getOtList, get_ot_leave_dayoff_List } from './get'
import { auth, register, tokenVerify } from './post'

dotenv.config()
const client = new Client({
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  database: process.env.DB_NAME,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD
})

client.connect()
const app = express()
app.use(bodyParser.json())
app.use(cors())

app.get('/getEmployeeList', async (req: Request, res: Response) => { getEmployeeList(req, res, client) })

//
app.get('/getUserInfo', async (req: Request, res: Response) => { getUserInfo(req, res, client) })
app.get('/getUserAccessibility', async (req: Request, res: Response) => { getUserAccessibility(req, res, client) })


app.get('/getGroupList', async (req: Request, res: Response) => { getGroupList(req, res, client) })

app.get('/getWorkDayList', async (req: Request, res: Response) => { getWorkDayList(req, res, client) })

app.get('/getOtList', async (req: Request, res: Response) => { getOtList(req, res, client) })

app.get('/get_ot_leave_dayoff_List', async (req: Request, res: Response) => { get_ot_leave_dayoff_List(req, res, client) })

app.post('/auth', async (req: Request, res: Response) => { auth(req, res, client) })

app.post('/register', async (req: Request, res: Response) => { register(req, res, client) })

app.post('/tokenVerify', async (req: Request, res: Response) => { tokenVerify(req, res, client) })

// default 
app.get('/', (req: Request, res: Response) => {
  res.send(`<h1>Backend server running on port ${process.env.HOST_PORT}</h1>`)
})

app.listen(process.env.HOST_PORT, () => {
  console.log(`[server]: Server is running at http://localhost:${process.env.HOST_PORT}`)
})
