import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'

const secretKey = 'asdsad'
export const auth = async (req: any, res: any, client: any) => {
    const { username, password } = req.body
    client.query(`SELECT * FROM authuser WHERE "USERNAME" = '${username}'`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'auth')
        } else {
            if (result.rows.length >= 1) {
                const outputPassword = result.rows[0].PASSWORD
                const hashedPassword = await bcrypt.compare(password, outputPassword)
                console.log('hashedPassword',hashedPassword)
                if (hashedPassword) {
                    const payload = {
                        user:result.rows[0].USERNAME,
                        password:result.rows[0].PASSWORD,
                        empId:result.rows[0].EMP_ID,
                        role:result.rows[0].SYSTEM_ROLE
                    }
                    res.json({status:'success', token:jwt.sign(payload, secretKey, { expiresIn: '1hr' })})
                }
            } else {
                res.json({status:'false'})
            }
        }
    })
}

export const register =  async (req: any, res: any, client: any) => {
    const { username, password } = req.body
    const hashedPassword = await bcrypt.hash(password, 10)
    client.query(`INSERT INTO authuser ("USERNAME","PASSWORD","EMP_ID","SYSTEM_ROLE")
    VALUES ('${username}', '${hashedPassword}','EMP_ID','admin')`, async (err: any, result: any) => {
        if (err) {
            console.error('Error executing query', err, 'register')
        } else {
            res.json(result.rows)
        }
    })
}

export const tokenVerify =  async (req: any, res: any, client: any) => {
    const { token } = req.body
    const decoded = jwt.verify(token, secretKey)
    console.log('decoded : ',decoded)
    res.json(decoded)
    // const hashedPassword = await bcrypt.hash(password, 10)
    // client.query(`INSERT INTO authuser ("USERNAME","PASSWORD","EMP_ID","SYSTEM_ROLE")
    // VALUES ('${username}', '${hashedPassword}','EMP_ID','admin')`, async (err: any, result: any) => {
    //     if (err) {
    //         console.error('Error executing query', err, 'register')
    //     } else {
    //         res.json(result.rows)
    //     }
    // })
}