import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../index'

export interface authenticationState {
    userInfo: any
    userAccessibility: { page: string, action: string[] }[] | []
}

const initialState: authenticationState = {
    userInfo: {},
    userAccessibility: []
}

export const authenticationSlice = createSlice({
    name: 'authentication',
    initialState,
    reducers: {
        setUserInfo: (state, action) => {
            state.userInfo = action.payload
        },
        setUserAccessibility: (state, action) => {
            state.userAccessibility = action.payload
        }
    }
})

export const { setUserInfo, setUserAccessibility } = authenticationSlice.actions
export default authenticationSlice.reducer
