import React, { useState } from 'react'
import { EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { useEffect } from 'react'
import CreateGroup from '@/components/Modals/createGroup'
import Table from '@/components/Table'
export async function getStaticProps() {
    const res = await fetch('http://localhost:8080/getGroupList/')
    const groupList = await res.json()
    return {
        props: {
            groupList: groupList
        },
    }
}

export default function GroupPage({ groupList }: any) {
    const [isOpenCreateModal, setisOpenCreateModal] = useState(false)
    useEffect(() => {
        console.log(groupList, 'groupList');

    }, [])
    const readFunc = (key: string) => {
        console.log('key', key)
    }
    const updateFunc = (key: string) => {
        console.log('key', key)
    }
    const deleteFunc = (key: string) => {
        console.log('key', key)
    }
    return (
        <div>groupPage
            <button
                onClick={() => { setisOpenCreateModal(true) }}
                className='flex justify-center py-[5px] w-[100px] text-violet-800 bg-violet-50 border border-violet-400 rounded-md'>
                <EyeOutlined />
            </button>

            <div className='bg-violet-100 border border-violet-400 rounded-lg'>
                <Table
                    columnTitle={['No.', 'Company', 'Group Name']}
                    fieldName={['COMPANY_ID', 'GROUP_NAME']}
                    data={groupList}
                    isActions={true}
                    actionKey={'GROUP_ID'}
                    readFunc={readFunc}
                    updateFunc={updateFunc}
                    deleteFunc={deleteFunc}
                    ActionsColumnWidth={'350px'}
                />

            </div>

            {/* <CreateGroup isOpen={isOpenCreateModal} openFunc={setisOpenCreateModal} /> */}
        </div>
    )
}
