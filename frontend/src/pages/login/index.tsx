import axios from 'axios'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useAppDispatch } from '@/store'
import { setUserInfo, setUserAccessibility } from '@/store/authentication'
export default function LoginPage() {
    const router = useRouter()
    const dispatch = useAppDispatch()
    const [username, setusername] = useState<string>('')
    const [password, setpassword] = useState<string>('')
    const login = () => {
        axios({
            method: 'post',
            url: 'http://localhost:8080/auth/',
            data: {
                username: username,
                password: password
            }
        }).then(function (response) {
            console.log('token: ', response)
            if (response.data.token) {
                sessionStorage.setItem('authResponse', JSON.stringify({ token: response.data.token }))
                // dispatch(setToken(response.data.token))
            }
        }).catch(function (response) {
            console.log('response', response)
        })
    }
    const getUserInfo = async (empId: string) => {
        await axios({
            method: 'get',
            url: 'http://localhost:8080/getUserInfo/',
            data: { empId }
        }).then(function (response) {
            dispatch(setUserInfo(response.data))
        }).catch(function (response) {
            console.log('response', response)
        })
    }
    const getUserAccessibility = async (empId: string) => {
        await axios({
            method: 'get',
            url: 'http://localhost:8080/getUserAccessibility/',
            data: { empId }
        }).then(function (response) {
            dispatch(setUserAccessibility(response.data))
        }).catch(function (response) {
            console.log('response', response)
        })
    }
    const verifyToken = async (token: string) => {
        axios({
            method: 'post',
            url: 'http://localhost:8080/tokenVerify/',
            data: {
                token: token,
            }
        }).then(function (response) {
            console.log('tokenVerify response : ', response.data.empId)
            // getUserInfo(response.data.empId)
            // getUserAccessibility(response.data.empId)
            router.push(`/myinfo`)
        })
    }
    useEffect(() => {
        const authInfo = sessionStorage.getItem('authResponse')
        if (authInfo !== null) {
            verifyToken(JSON.parse(authInfo).token)
        }
    }, [])
    return (
        <div className=' absolute left-[0px] top-[0px] w-[100%] h-[100%] bg-violet-200 pt-[150px]'>
            <div className='flex justify-center w-[100%]'>
                <div className='border-2 rounded-lg w-[50%] p-[20px] bg-violet-300 shadow-lg shadow-gray-500'>
                    <div className='mb-[10px]'>
                        <label htmlFor='username' className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>Username</label>
                        <input value={username} onChange={(e) => { setusername(e.target.value) }} type='email' id='username' className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' placeholder='username' />
                    </div>
                    <div className='mb-[10px]'>
                        <label htmlFor='password' className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>Password</label>
                        <input value={password} onChange={(e) => { setpassword(e.target.value) }} type='password' id='password' placeholder='••••••••' className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' />
                    </div>
                    <div className='mb-[10px]'>
                        <p>Forget your password ?</p>
                    </div>
                    <button className='border-2 w-[100%] h-[40px] rounded-lg bg-violet-200' onClick={login}>login</button>
                </div>
            </div>
        </div>
    )
}