import { useEffect } from "react"

export async function getStaticProps() {
    // const res = await fetch('https://jsonplaceholder.typicode.com/posts')
    // const posts = await res.json()
    const res = await fetch('http://localhost:3000/api/hello')
    const response = await res.json()

    return {
        props: { response }
    }
}
export default function MyinfoPage(props: any) {
    useEffect(() => {
        console.log(props)
    }, [])
    return (
        <div>
            <div className='border-2 flex flex-col w-[30%]'>
                Myinfo
                <div>
                    <div>full name</div>
                    <div>Mr. Witchapart Sangmuang</div>
                </div>
                <div>
                    <div>Email</div>
                    <div>Witchapart.s@mail.com</div>
                </div>
                <div>
                    <div>username</div>
                    <div>wi.sang</div>
                </div>
                <div>
                    <div>password</div>
                    <div>********</div>
                    <div>change password</div>
                    <div>hidden</div>
                </div>
            </div>
            <div className='border-2 flex flex-col w-[30%]'>
                additional information
                <div>
                    <div>age</div>
                    <div>24</div>
                </div>
                <div>
                    <div>birth date</div>
                    <div>12 Jan 1999</div>
                </div>
                <div>
                    <div>education</div>
                    <div>KMITL</div>
                </div>
            </div>
            {/* <form>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">emp ID</span>
                    <input type="text" className="border border-slate-200 focus:outline-none rounded-md w-[200px]" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">ID card number</span>
                    <input type="text" className="border border-slate-200 focus:outline-none rounded-md w-[200px]" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Firstname</span>
                    <input type="text" className="border border-slate-200 focus:outline-none rounded-md w-[200px]" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Lastname</span>
                    <input type="text" className="border border-slate-200 focus:outline-none rounded-md  w-[200px]" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Birthdate</span>
                    <input type="text" className="border border-slate-200 focus:outline-none rounded-md  w-[200px]" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Age</span>
                    <input type="date" className="border border-slate-200 focus:outline-none rounded-md  w-[200px]" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Current address </span>
                    <textarea className="border border-slate-200 focus:outline-none rounded-md w-[200px] h-[60px] resize-none" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Address on ID card</span>
                    <textarea className="border border-slate-200 focus:outline-none rounded-md w-[200px] h-[60px] resize-none" />
                </label>
                <label className="block">
                    <span className="block text-sm font-medium text-slate-700">Phone number</span>
                    <input type="date" className="border border-slate-200 focus:outline-none rounded-md  w-[200px]" />
                </label>
            </form> */}

        </div>
    )
}