import Calendar from "@/components/Calendar";
import TableModal from "@/components/Modal/TableModal";
import { useEffect, useState } from "react";

interface dateEvent {
    date: string
    eventList: { empName: string, type: string }[]
}

export default function TimesheetPage() {
    const [yearPicker, setyearPicker] = useState<string>(new Date().getFullYear().toString())
    const [monthPicker, setmonthPicker] = useState<string>(new Date().getMonth() + 1 >= 10 ? `${new Date().getMonth() + 1}` : `0${new Date().getMonth() + 1}`)
    const [calendarSlot, setcalendarSlot] = useState<Date[]>([])
    const [dateEventList, setdateEventList] = useState<dateEvent[]>([])
    const [isOpenModal, setisOpenModal] = useState(false)

    const getDateInfo = async () => {
        const res = await fetch('http://localhost:8080/getWorkDayList/')
        setdateEventList(await res.json())
    }

    const changePicker = (picker: string) => {
        setyearPicker(picker.split('-')[0])
        setmonthPicker(picker.split('-')[1])
    }
    const getAllDatesInMonth = (year: number, month: number) => {
        let endDateLastMonth = new Date(year, month - 1, 0)
        let startDateCurrentMonth = new Date(year, month - 1, 1)
        let startDateNextMonth = new Date(year, month, 1)
        let datelist = []
        if (startDateCurrentMonth.getDay() !== 0) {
            while (endDateLastMonth.getDay() - 1 >= 0) {
                datelist.unshift(new Date(endDateLastMonth))
                endDateLastMonth.setDate(endDateLastMonth.getDate() - 1)
            }
            datelist.unshift(new Date(endDateLastMonth))
        }
        while (startDateCurrentMonth.getMonth() === month - 1) {
            datelist.push(new Date(startDateCurrentMonth))
            startDateCurrentMonth.setDate(startDateCurrentMonth.getDate() + 1)
        }
        if (startDateNextMonth.getDay() !== 0) {
            while (startDateNextMonth.getDay() + 1 <= 6) {
                datelist.push(new Date(startDateNextMonth))
                startDateNextMonth.setDate(startDateNextMonth.getDate() + 1)
            }
            datelist.push(new Date(startDateNextMonth))
        }
        return datelist
    }
    const [dateInfo, setdateInfo] = useState<dateEvent | null>(null)
    const dateDetail = (detail: any) => {
        if (detail !== null) {
            setdateInfo(detail)
            setisOpenModal(true)
        }
    }
    const closeModal = () => {
        setdateInfo(null)
        setisOpenModal(false)
    }
    const readFunc = (key: string) => {
        console.log('key', key)
    }
    const updateFunc = (key: string) => {
        console.log('key', key)
    }
    const deleteFunc = (key: string) => {
        console.log('key', key)
    }

    useEffect(() => {
        // getDateInfo()
        setdateEventList(
            [
                {
                    date: '2023-11-13',
                    eventList: [
                        {
                            empName: 'Witchapart S.',
                            type: 'OT'
                        },
                        {
                            empName: 'Siri A.',
                            type: 'OT'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'OT'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'OT'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'Sick Leave Without Pay Leave'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'Sick Leave Without Pay Leave'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'OT'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'OT'
                        },
                        {
                            empName: 'Sopin K.',
                            type: 'OT'
                        }
                    ]
                },
                {
                    date: '2023-11-14',
                    eventList: [
                        {
                            empName: 'Witchapart S.',
                            type: 'OT'
                        },
                    ]
                }
            ])
    }, [])
    useEffect(() => {
        console.log(dateEventList)
    }, [dateEventList])
    useEffect(() => {
        setcalendarSlot(getAllDatesInMonth(Number(yearPicker), Number(monthPicker)))
    }, [yearPicker, monthPicker])

    return (
        <div>Timesheet
            <div className="flex pb-[20px] px-[20px] justify-end">
                <input className="h-[40px] p-[5px] border-[2px] rounded-md border-violet-400" type="month" onChange={(e) => { changePicker(e.target.value) }} value={`${yearPicker}-${monthPicker}`} />
            </div>
            {
                isOpenModal === true &&
                <TableModal
                    modalTitle={dateInfo !== null ? dateInfo.date : ''}
                    columnTitle={['No.', 'empName', 'type', 'Actions']}
                    fieldName={['empName', 'type']}
                    data={dateInfo !== null ? dateInfo.eventList : []}
                    isActions={true}
                    actionKey={'EMP_ID'}
                    readFunc={readFunc}
                    updateFunc={updateFunc}
                    deleteFunc={deleteFunc}
                    closeModal={closeModal}
                />
            }
            {/* <Calendar monthPicker={monthPicker} calendarSlot={calendarSlot} dateEventList={dateEventList} dateDetailFunc={dateDetail} /> */}
        </div>
    )
}