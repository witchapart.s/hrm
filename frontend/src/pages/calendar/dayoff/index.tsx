import Calendar from "@/components/Calendar"
import TableModal from "@/components/Modal/TableModal"
import ToggleSwitch from '@/components/ToggleSwitch'
import { useEffect, useState } from "react"
interface dateEvent {
    SK: string
    DATE: string
    START_TIME: string
    END_TIME: string
    HOURS: number
    EMP_ID: string
    TYPE: string
    STATUS: string
    FIRST_NAME_TH: string
    LAST_NAME_TH: string
}

export default function DayoffPage() {


    // START use for filter
    const [yearPicker, setyearPicker] = useState<string>(new Date().getFullYear().toString())
    const [monthPicker, setmonthPicker] = useState<string>(new Date().getMonth() + 1 >= 10 ? `${new Date().getMonth() + 1}` : `0${new Date().getMonth() + 1}`)
    const [otCheck, setotCheck] = useState(true)
    const [leaveCheck, setleaveCheck] = useState(true)
    const [dayoffCheck, setdayoffCheck] = useState(true)
    // END use for filter

    // START data display
    const [calendarSlot, setcalendarSlot] = useState<Date[]>([])
    const [dateEventList, setdateEventList] = useState<dateEvent[]>([])
    const [filterDateEventList, setfilterDateEventList] = useState<dateEvent[]>([])
    // END data display

    // START about Modal
    const [dateInfo, setdateInfo] = useState<any>(null)
    const [isOpenModal, setisOpenModal] = useState(false)
    const dateDetail = (detail: any) => {
        if (detail !== null) {
            setdateInfo(detail)
            setisOpenModal(true)
        }
    }
    const closeModal = () => {
        setdateInfo(null)
        setisOpenModal(false)
    }
    // END about Modal

    // START transform
    const changePicker = (picker: string) => {
        setyearPicker(picker.split('-')[0])
        setmonthPicker(picker.split('-')[1])
    }
    const getAllDatesInMonth = (year: number, month: number) => {
        let endDateLastMonth = new Date(year, month - 1, 0)
        let startDateCurrentMonth = new Date(year, month - 1, 1)
        let startDateNextMonth = new Date(year, month, 1)
        let datelist = []
        if (startDateCurrentMonth.getDay() !== 0) {
            while (endDateLastMonth.getDay() - 1 >= 0) {
                datelist.unshift(new Date(endDateLastMonth))
                endDateLastMonth.setDate(endDateLastMonth.getDate() - 1)
            }
            datelist.unshift(new Date(endDateLastMonth))
        }
        while (startDateCurrentMonth.getMonth() === month - 1) {
            datelist.push(new Date(startDateCurrentMonth))
            startDateCurrentMonth.setDate(startDateCurrentMonth.getDate() + 1)
        }
        if (startDateNextMonth.getDay() !== 0) {
            while (startDateNextMonth.getDay() + 1 <= 6) {
                datelist.push(new Date(startDateNextMonth))
                startDateNextMonth.setDate(startDateNextMonth.getDate() + 1)
            }
            datelist.push(new Date(startDateNextMonth))
        }
        return datelist
    }
    const formatDate = (date: Date) => {
        var d = new Date(date)
        var month = '' + (d.getMonth() + 1)
        var day = '' + d.getDate()
        var year = d.getFullYear()
        if (month.length < 2)
            month = '0' + month
        if (day.length < 2)
            day = '0' + day
        return [year, month, day].join('-')
    }
    // END transform

    // START API
    const getDateInfo = async (startDate: Date, endDate: Date) => {
        console.log(startDate, endDate, 'startDate endDate');

        const res = await fetch('http://localhost:8080/get_ot_leave_dayoff_List/?' + new URLSearchParams({
            startDate: formatDate(startDate),
            endDate: formatDate(endDate)
        }))
        // console.log(await res.json(), 'res')
        setdateEventList(await res.json())
    }
    // END API
    
    

    
    const readFunc = (key: string) => {
        console.log('key', key)
    }
    const updateFunc = (key: string) => {
        console.log('key', key)
    }
    const deleteFunc = (key: string) => {
        console.log('key', key)
    }





    // START useEffect()
    useEffect(() => {
        console.log(yearPicker, monthPicker, 'yearPicker, monthPicker')
        setcalendarSlot(getAllDatesInMonth(Number(yearPicker), Number(monthPicker)))
    }, [yearPicker, monthPicker])
    useEffect(() => {
        console.log(calendarSlot, 'calendarSlot');
        getDateInfo(calendarSlot[0], calendarSlot[calendarSlot.length - 1])
    }, [calendarSlot])
    useEffect(() => {
        console.log(dateEventList)
        setfilterDateEventList(dateEventList)
    }, [dateEventList])
    useEffect(() => {
        setfilterDateEventList([])
        if (otCheck) {
            setfilterDateEventList(filterDateEventList => [...filterDateEventList, ...dateEventList.filter((obj) => obj.TYPE === 'OT')])
        }
        if (leaveCheck) {
            setfilterDateEventList(filterDateEventList => [...filterDateEventList, ...dateEventList.filter((obj) => obj.TYPE.includes('Leave'))])
        }
        if (dayoffCheck) {
            setfilterDateEventList(filterDateEventList => [...filterDateEventList, ...dateEventList.filter((obj) => obj.TYPE.includes('Day off'))])
        }
    }, [otCheck, leaveCheck, dayoffCheck])
    // END useEffect()

    return (
        <div>OT/Leave/Dayoff
            <div className="flex pb-[20px] px-[20px] justify-end">
                <ul className='flex flex-wrap pt-[10px] pl-[40px]'>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>OT</span><span className='flex items-center'><ToggleSwitch value={otCheck} funcCheck={setotCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Leave</span><span className='flex items-center'><ToggleSwitch value={leaveCheck} funcCheck={setleaveCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Dayoff</span><span className='flex items-center'><ToggleSwitch value={dayoffCheck} funcCheck={setdayoffCheck} /></span></li>
                </ul>
                <input className="h-[40px] p-[5px] border-[2px] rounded-md border-violet-400" type="month" onChange={(e) => { changePicker(e.target.value) }} value={`${yearPicker}-${monthPicker}`} />
            </div>
            {
                isOpenModal === true &&
                <TableModal
                    modalTitle={dateInfo !== null ? dateInfo.date : ''}
                    columnTitle={['No.', 'First Name', 'Last Name', 'Type','Hours','Start time','End time']}
                    fieldName={['FIRST_NAME_TH', 'LAST_NAME_TH', 'TYPE','HOURS','START_TIME','END_TIME']}
                    data={dateInfo}
                    // isActions={true}
                    // actionKey={'EMP_ID'}
                    // readFunc={readFunc}
                    // updateFunc={updateFunc}
                    // deleteFunc={deleteFunc}
                    // ActionsColumnWidth={'400px'}
                    closeModal={closeModal}
                />
            }
            <Calendar monthPicker={monthPicker} calendarSlot={calendarSlot} dateEventList={filterDateEventList} dateDetailFunc={dateDetail} />
        </div>
    )
}