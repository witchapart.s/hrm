import axios from 'axios'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
export default function RegisterPage() {
    const router = useRouter()
    const [username, setusername] = useState<string>('')
    const [password, setpassword] = useState<string>('')
    const [confirmPassword, setconfirmPassword] = useState<string>('')
    const [errorPasswordNotMatch, seterrorPasswordNotMatch] = useState<boolean>(true)
    // const [token, settoken] = useState('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiYXNkIiwicGFzc3dvcmQiOiIkMmIkMTAkM0NROFlTOGZRY1Y5UlZ4dGF5MHE3LnJpYnJaZmtocUJRZjU3anRrYjZsMS5ZWGY5REFTdm0iLCJyb2xlIjoiYWRtaW4iLCJpYXQiOjE3MDA0OTA4MDUsImV4cCI6MTcwMDQ5NDQwNX0.aS8Q4oAw2EJpM88ExrOlQCsvn_195M37QK8zBdUF9kw')
    // const [info, setinfo] = useState<any>()
    // const login = () => {
    //     axios({
    //         method: 'post',
    //         url: 'http://localhost:8080/auth/',
    //         data: {
    //             username: username,
    //             password: password
    //         }
    //     }).then(function (response) {
    //         setinfo(response)
    //     })
    // }

    const register = () => {
        if (password === confirmPassword) {
            seterrorPasswordNotMatch(true)
            axios({
                method: 'post',
                url: 'http://localhost:8080/register/',
                data: {
                    username: username,
                    password: password
                }
            }).then(function (response) {
                router.push(`/login`)
            })
        } else {
            seterrorPasswordNotMatch(false)
        }
    }

    // const verifyToken = () => {
    //     axios({
    //         method: 'post',
    //         url: 'http://localhost:8080/tokenVerify/',
    //         data: {
    //             token: token,
    //         }
    //     }).then(function (response) {
    //         console.log('tokenVerify response : ', response)

    //     })
    // }
    // useEffect(() => {
    //     console.log('info', info)
    // }, [info])
    return (
        <div className=' absolute left-[0px] top-[0px] w-[100%] h-[100%] bg-violet-400 pt-[150px]'>
            <div className='flex justify-center w-[100%]'>
                <div className='border-2 rounded-lg w-[50%] p-[20px]'>
                    <div className='mb-[10px]'>
                        <label htmlFor='username' className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>Username</label>
                        <input value={username} onChange={(e) => { setusername(e.target.value) }} type='text' id='username' className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' placeholder='username' />
                    </div>
                    <div className='mb-[10px]'>
                        <label htmlFor='password' className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>Password</label>
                        <input value={password} onChange={(e) => { setpassword(e.target.value) }} type='password' id='password' placeholder='••••••••' className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' />
                    </div>
                    <div className='mb-[10px]'>
                        <label htmlFor='confirmPassword' className='block mb-2 text-sm font-medium text-gray-900 dark:text-white'>Confirm Password</label>
                        <input value={confirmPassword} onChange={(e) => { setconfirmPassword(e.target.value) }} type='password' id='confirmPassword' placeholder='••••••••' className='bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500' />
                        <p className={'text-red-600 ' + (errorPasswordNotMatch ? 'hidden' : 'block')}>your password is not matched!</p>
                    </div>
                    <button className='border-2 rounded-lg bg-violet-200' onClick={register}>register</button>
                    {/* <button className='border-2 rounded-lg bg-violet-200' onClick={verifyToken}>token verify</button> */}
                </div>
            </div>
        </div>
    )
}