import React, { useEffect, useState } from 'react'

import Table from '@/components/Table'

export async function getStaticProps() {
    const res = await fetch('http://localhost:8080/getEmployeeList/')
    const empList = await res.json()
    return {
        props: {
            empList: empList
        },
    }
}

export default function EmployeesPage({ empList }: any) {
    const [search, setsearch] = useState<string>('')
    const [filterData, setfilterData] = useState(empList)
    const handleSearch = () => {
        if (search !== '') {
            setfilterData(empList.filter((obj: any) => (obj.EMP_ID.includes(search) || obj.INTERNAL_EMP_ID.includes(search) || obj.FIRST_NAME_TH.includes(search) || obj.LAST_NAME_TH.includes(search))))
        } else {
            setfilterData(empList)
        }
    }

    const readFunc = (key: string) => {
        console.log('key', key)
    }
    const updateFunc = (key: string) => {
        console.log('key', key)
    }
    const deleteFunc = (key: string) => {
        console.log('key', key)
    }


    return (
        <div>
            <div className='flex px-[20px] mb-[20px]'>
                <label className='h-[35px]'>
                    <span>Search : </span>
                    <input type='text'
                        className='border rounded h-[100%] focus:outline-none focus:border-sky-500 pl-[10px]'
                        placeholder='find something!'
                        value={search}
                        onChange={(e) => { setsearch(e.target.value) }}
                        onKeyUp={handleSearch}
                    />
                </label>
            </div>

            <div className='bg-violet-100 border border-violet-400 rounded-lg'>
                <Table
                    columnTitle={['No.', 'EMP_ID', 'INTERNAL_EMP_ID', 'FIRST_NAME_TH', 'LAST_NAME_TH', 'INDENTIFICATION_NUMBER']}
                    fieldName={['EMP_ID', 'INTERNAL_EMP_ID', 'FIRST_NAME_TH', 'LAST_NAME_TH', 'INDENTIFICATION_NUMBER']}
                    data={filterData}
                    isActions={true}
                    actionKey={'EMP_ID'}
                    readFunc={readFunc}
                    updateFunc={updateFunc}
                    deleteFunc={deleteFunc}
                    ActionsColumnWidth={'350px'}
                />
            </div>
        </div>
    )
}

