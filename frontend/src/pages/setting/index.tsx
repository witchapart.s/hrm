import DataTableDisplay from '@/components/DataTableDisplay'
import ToggleSwitch from '@/components/ToggleSwitch'
import React, { useEffect, useState } from 'react'
export default function SettingPage() {
    const [mondayCheck, setmondayCheck] = useState(false)
    const [sundayCheck, setsundayCheck] = useState(false)
    const [tuesdayCheck, settuesdayCheck] = useState(false)
    const [wednesdayCheck, setwednesdayCheck] = useState(false)
    const [thursdayCheck, setthursdayCheck] = useState(false)
    const [fridayCheck, setfridayCheck] = useState(false)
    const [saturdayCheck, setsaturdayCheck] = useState(false)

    const infoFunc = (key: string) => {
        console.log('infoFunc key', key)
    }
    const editFunc = (key: string) => {
        console.log('editFunc key', key)
    }
    const deleteFunc = (key: string) => {
        console.log('deleteFunc key', key)
    }

    return (
        <>
            <div className='border border-violet-400 rounded-md mb-[10px]'>
                <span className='block pt-[15px] pl-[20px]'>Defualt day off :</span>
                <ul className='flex flex-wrap pt-[10px] pl-[40px]'>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Sunday</span><span className='flex items-center'><ToggleSwitch value={sundayCheck} funcCheck={setsundayCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Monday</span><span className='flex items-center'><ToggleSwitch value={mondayCheck} funcCheck={setmondayCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Tuesday</span><span className='flex items-center'><ToggleSwitch value={tuesdayCheck} funcCheck={settuesdayCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Wednesday</span><span className='flex items-center'><ToggleSwitch value={wednesdayCheck} funcCheck={setwednesdayCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Thursday</span><span className='flex items-center'><ToggleSwitch value={thursdayCheck} funcCheck={setthursdayCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Friday</span><span className='flex items-center'><ToggleSwitch value={fridayCheck} funcCheck={setfridayCheck} /></span></li>
                    <li className='flex h-[50px] mr-[25px]'><span className='flex items-center mr-[5px]'>Saturday</span><span className='flex items-center'><ToggleSwitch value={saturdayCheck} funcCheck={setsaturdayCheck} /></span></li>
                </ul>
            </div>
            <div className='border border-violet-400 rounded-md mb-[10px]'>
                <span className='block pt-[15px] pl-[20px]'>Corporate day off :</span>
                <div className='p-[20px]'>
                    <DataTableDisplay
                        title={['Date', 'Name', 'Descroption', 'Actions']}
                        data={[
                            {
                                date: '12/08/2564',
                                name: 'Mother Day',
                                desc: 'Mother Day'
                            },
                            {
                                date: '05/12/2564',
                                name: 'Father Day',
                                desc: 'Father Day'
                            }
                        ]}
                        infoFunc={infoFunc}
                        editFunc={editFunc}
                        deleteFunc={deleteFunc}
                    />
                </div>
            </div>
        </>
    )
}