import styles from '@/components/ToggleSwitch/index.module.css'
import React, { useEffect, useState } from 'react'
export default function ToggleSwitch(props:{value:boolean,funcCheck:Function}) {
    const { value, funcCheck } = props
    return (
        <label className='relative inline-block w-[60px] h-[30px]'>
            <input
                type='checkbox'
                className={`peer hidden`}
                checked={value}
                onChange={() => { funcCheck(!value) }}
            />
            <span className={`peer-checked/:bg-violet-500 peer-checked/:before:translate-x-7 absolute cursor-pointer bg-[#ccc] transition-[0.4s] rounded-[34px] inset-0 before:absolute before:content-[''] before:h-6 before:w-6 before:bg-[white] before:transition-[0.4s] before:rounded-[50%] before:left-1 before:bottom-[3px]`}></span>
        </label>
    )
}
