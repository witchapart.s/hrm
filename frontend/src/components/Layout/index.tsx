import React, { ReactNode } from 'react';
import Sidebar from '../Sidebar';
interface LayoutProps {
    children: ReactNode;
}
export default function Layout({ children }: LayoutProps) {
    return (
        <div className='flex'>
            <div className='fixed p-[10px] h-[100%] w-[300px] bg-violet-100'>
                <Sidebar />
            </div>
            <div className='p-[10px] h-[100%] w-[100%] ml-[300px]'>
                {children}
            </div>
        </div>
    )
}