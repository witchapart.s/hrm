import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { CalendarOutlined, SettingOutlined, DashboardOutlined, FileTextOutlined, AuditOutlined, IdcardOutlined, AppstoreOutlined} from '@ant-design/icons'

// import SidebarButton from '../SidebarButton'
export default function Sidebar() {
    const router = useRouter()
    
    useEffect(() => {
        console.log(router.pathname)
    }, [router])
    return (
        <ul>
            <li className={`flex mb-[3px] h-[40px] text-lg ${router.pathname === '/myinfo' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/myinfo`) }}><IdcardOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>My infomation</span></li>
            <li className={`flex mb-[3px] h-[40px] text-lg ${router.pathname === '/dashboard' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/dashboard`) }}><DashboardOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>Dashbaord</span></li>
            
            <span className='flex h-[40px] text-lg block bg-violet-700'><CalendarOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>Calendar</span></span>
            <ul className='mb-[3px]'>
                <li className={`flex h-[40px] text-lg pl-[35px] ${router.pathname === '/calendar/timesheet' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/calendar/timesheet`) }}><span className='pt-[6px] text-[14px]'>Timesheet</span></li>
                <li className={`flex h-[40px] text-lg pl-[35px] ${router.pathname === '/calendar/dayoff' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/calendar/dayoff`) }}><span className='pt-[6px] text-[14px]'>Dayoff</span></li>
            </ul>
            
            <span className='flex  h-[40px] text-lg block bg-violet-700'><AuditOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>HR Menus</span></span>
            <ul className='mb-[3px]'>
                <li className={`flex h-[40px] text-lg pl-[35px] ${router.pathname === '/employees' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/employees`) }}><span className='pt-[6px] text-[14px]'>Employees</span></li>
                <li className={`flex h-[40px] text-lg pl-[35px] ${router.pathname === '/salarypays' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/salarypays`) }}><span className='pt-[6px] text-[14px]'>Salary pays</span></li>
            </ul>
            <li className={`flex mb-[3px] h-[40px] text-lg ${router.pathname === '/inventory' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/inventory`) }}><AppstoreOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>Inventory</span></li>
            <li className={`flex mb-[3px] h-[40px] text-lg ${router.pathname === '/setting' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/setting`) }}><SettingOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>Setting</span></li>
            
            <li className={`flex mb-[3px] h-[40px] text-lg ${router.pathname === '/group_setting' ? 'bg-violet-500' : 'bg-violet-400'}`} onClick={() => { router.push(`/group_setting`) }}><SettingOutlined className='text-[20px] ml-[20px] mr-[10px] pt-[10px]' /><span className='pt-[6px] text-[14px]'>group_setting</span></li>
        
        </ul>




    )
}