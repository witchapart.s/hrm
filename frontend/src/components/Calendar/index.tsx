import { use, useEffect, useState } from "react"
import TableModal from "../Modal/TableModal"

interface dateEvent {
    SK: string
    DATE: string
    START_TIME: string
    END_TIME: string
    HOURS: number
    EMP_ID: string
    TYPE: string
    STATUS: string
    FIRST_NAME_TH: string
    LAST_NAME_TH: string
}
interface Props {
    monthPicker: string
    calendarSlot: Date[]
    dateEventList: dateEvent[]
    dateDetailFunc: Function
}

export default function Calendar(props: Props) {
    const { monthPicker, calendarSlot, dateEventList, dateDetailFunc } = props
    const formatDate = (date: any) => {
        var d = new Date(date)
        var month = '' + (d.getMonth() + 1)
        var day = '' + d.getDate()
        var year = d.getFullYear()
        if (month.length < 2)
            month = '0' + month
        if (day.length < 2)
            day = '0' + day
        return [year, month, day].join('-')
    }

    const formatDatetimeToDate = (date: string) => {
        return date.split('T')[0]
    }

    // useEffect(()=>{
    //    console.log(formatDate("2023-11-14T00:00:00.000Z"),'formatDate2("2023-11-14T00:00:00.000Z")')
    // },[])

    return (
        <>
            <div className="px-[20px]">
                <div className="flex w-[100%] pt-[5px] bg-violet-300 rounded-t-lg">
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">SUN</div>
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">MON</div>
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">TUE</div>
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">WED</div>
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">THU</div>
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">FRI</div>
                    <div className="bg-violet-300 font-semibold w-[14.285%] h-[40px] text-center pt-[5px]">SAT</div>
                </div>
                <div className="flex w-[100%] flex-wrap pb-[10px] bg-violet-300 rounded-b-lg">
                    {
                        calendarSlot.map((slot, index) => (
                            <div
                                onClick={() => { dateDetailFunc(dateEventList.filter((obj) => formatDatetimeToDate(obj.DATE) === formatDate(slot)).length > 0 ? dateEventList.filter((obj) => formatDatetimeToDate(obj.DATE) === formatDate(slot)) : null) }}
                                key={index}
                                className={`border bg-violet-100 relative w-[14.285%] h-[100px] text-center ${Number(monthPicker) !== Number(slot.getMonth() + 1) ? 'cursor-default bg-[#F2F2F2]' : 'cursor-pointer'}`}>
                                <span className="opacity-50 text-white block top-[2px] right-[2px] absolute bg-violet-300 rounded-full p-[2px] w-[30px] h-[30px]">{slot.getDate()}</span>
                                <div className="overflow-auto w-[100%] h-[100%] flex flex-wrap pt-[2px]">
                                    {
                                        dateEventList.filter((obj) => formatDatetimeToDate(obj.DATE) === formatDate(slot)).length !== 0 ?
                                            dateEventList.filter((obj) => formatDatetimeToDate(obj.DATE) === formatDate(slot)).map((obj) => (
                                                <div className="border-[1px] flex bg-[#f8fafc] max-w-full h-[22px] pt-[3px] pl-[3px] pr-[3px]">
                                                    <p className="text-[10px] truncate">{obj.FIRST_NAME_TH} {obj.LAST_NAME_TH}</p>
                                                    <p className="text-[10px] truncate pl-[1px]">({obj.TYPE})</p>
                                                </div>
                                            ))
                                            : 'No Data'
                                    }
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        </>
    )
}