import { EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { useEffect } from 'react'

interface Props {
    title: string[]
    data: { [key: string]: string | number }[]
    infoFunc: Function
    editFunc: Function
    deleteFunc: Function
}

export default function DataTableDisplay(props: Props) {

    const { title, data, infoFunc, editFunc, deleteFunc } = props

    return (
        <div className='bg-violet-100 border border-violet-400 rounded-lg'>
            <table className='w-[100%] mt-[10px] mb-[20px] table-auto'>
                <thead>
                    <tr>
                        {
                            title.map((title: string, index: number) => (
                                <th className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]' key={`title-${index}-${title}`}>{title}</th>
                            ))
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map((rowData, index: number) => (
                            <tr key={index} className='bg-violet-100 hover:bg-violet-200'>
                                {Object.keys(rowData).map((key: string) => (
                                    <td className='border-b border-white text-center' key={`${index}-${key}`}>{rowData[key]}</td>
                                ))}
                                <td className='border-b border-white flex justify-around' key={`${index}-actions`}>
                                    <button onClick={() => { infoFunc(rowData[Object.keys(rowData)[0]]) }} className='flex justify-center py-[5px] w-[100px] text-violet-800 bg-violet-50 border border-violet-400 rounded-md'><EyeOutlined /></button>
                                    <button onClick={() => { editFunc(rowData[Object.keys(rowData)[0]]) }} className='flex justify-center py-[5px] w-[100px] text-gray-800 bg-gray-50 border border-gray-400 rounded-md'><EditOutlined /></button>
                                    <button onClick={() => { deleteFunc(rowData[Object.keys(rowData)[0]]) }} className='flex justify-center py-[5px] w-[100px] text-red-800 bg-red-50 border border-red-400 rounded-md'><DeleteOutlined /></button>
                                </td>
                            </tr>
                        ))}
                </tbody>
            </table>
        </div>
    )
}
