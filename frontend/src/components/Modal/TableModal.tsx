import { useEffect } from "react"
import { EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'

interface props {
    modalTitle: string
    columnTitle: string[]
    fieldName: string[]
    data: {}[]
    isActions?: boolean
    actionKey?: string
    readFunc?: Function
    updateFunc?: Function
    deleteFunc?: Function
    ActionsColumnWidth?: string
    closeModal: Function
}

export default function TableModal(props: props) {
    const { modalTitle, columnTitle, fieldName, data, isActions, actionKey, readFunc, updateFunc, deleteFunc, ActionsColumnWidth, closeModal } = props
    useEffect(() => {
        console.log(data, 'data')
    }, [])

    return (
        <div className='flex justify-center absolute top-0 left-0 w-[100%] h-[100%] opacity-100'>
            {/* backdrop */}
            <div className='flex justify-center absolute top-0 left-0 w-[100%] h-[100%] bg-black z-[100] opacity-75'></div>
            {/* Modal Content */}
            <div className='absolute opacity-100 z-40 h-auto mt-[150px] w-[80%] bg-[#ffffff] rounded-md p-[15px] z-[110]'>
                {/* Header */}
                <div className="flex items-center justify-between p-[5px] border-b rounded-t dark:border-gray-600">
                    <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
                        {modalTitle}
                    </h3>
                    <button onClick={() => { closeModal(false) }} type="button" className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white">
                        X
                    </button>
                </div>
                {/* Body */}
                <div className="p-4">
                    <div className='bg-violet-100 border border-violet-400 rounded-lg'>
                        <table className='w-[100%] mt-[10px] mb-[20px] table-auto'>
                            <thead>
                                <tr className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]'>
                                    {
                                        columnTitle.map((title: string, index: number) => (
                                            <th className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]'>{title}</th>
                                        ))
                                    }
                                    {
                                        isActions && <th key={`${columnTitle.length}-Actions`} style={{ width: ActionsColumnWidth !== undefined ? ActionsColumnWidth : 'auto' }} className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]'>Actions</th>
                                    }
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    data.map((rowData: any, index: number) => (
                                        <tr key={index} className='bg-violet-100 hover:bg-violet-200'>
                                            <td className='border-b border-white text-center' key={`${index}`}>{index + 1}</td>
                                            {
                                                fieldName.map((field: string) => (
                                                    <td className='border-b border-white text-center' key={`${index}-${field}`}>{rowData[field]}</td>
                                                ))
                                            }
                                            {
                                                isActions &&
                                                <td key={`${index}-actions`} className='border-b border-white flex justify-around' style={{ width: ActionsColumnWidth !== undefined ? ActionsColumnWidth : 'auto' }}>
                                                    {readFunc !== undefined && actionKey !== undefined && <button onClick={() => { readFunc(rowData[actionKey]) }} className='flex justify-center py-[5px] w-[100px] text-violet-800 bg-violet-50 border border-violet-400 rounded-md'><EyeOutlined /></button>}
                                                    {updateFunc !== undefined && actionKey !== undefined && <button onClick={() => { updateFunc(rowData[actionKey]) }} className='flex justify-center py-[5px] w-[100px] text-gray-800 bg-gray-50 border border-gray-400 rounded-md'><EditOutlined /></button>}
                                                    {deleteFunc !== undefined && actionKey !== undefined && <button onClick={() => { deleteFunc(rowData[actionKey]) }} className='flex justify-center py-[5px] w-[100px] text-red-800 bg-red-50 border border-red-400 rounded-md'><DeleteOutlined /></button>}
                                                </td>
                                            }
                                        </tr>
                                    ))}
                            </tbody>
                        </table>
                    </div>
                </div>
                {/* Footer */}
                <div className="flex items-center p-[5px] border-t border-gray-200 rounded-b dark:border-gray-600">
                    <button onClick={() => { closeModal(false) }} type="button" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">I accept</button>
                    <button onClick={() => { closeModal(false) }} type="button" className="ms-3 text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">Decline</button>
                </div>
            </div>
        </div>
    )
}