import { EyeOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { useEffect } from 'react'


interface Props {
    columnTitle: string[]
    fieldName: string[]
    data: []
    isActions?: boolean
    actionKey?: string
    readFunc?: Function
    updateFunc?: Function
    deleteFunc?: Function
    ActionsColumnWidth?: string
}
export default function Table(props: Props) {
    const { columnTitle, fieldName, data, isActions, actionKey, readFunc, updateFunc, deleteFunc, ActionsColumnWidth } = props
    return (
        <table className='w-[100%] mt-[10px] mb-[20px] table-auto'>
            <thead>
                <tr className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]'>
                    {
                        columnTitle.map((title: string, index: number) => (
                            <th key={`${index}-${title}`} className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]'>{title}</th>
                        ))
                    }
                    {
                        isActions && <th key={`${columnTitle.length}-Actions`} style={{ width: ActionsColumnWidth !== undefined ? ActionsColumnWidth : 'auto' }} className='border-b border-violet-500 bg-violet-100 text-center pb-[10px]'>Actions</th>
                    }
                </tr>
            </thead>
            <tbody>
                {
                    data.map((rowData: any, index: number) => (
                        <tr key={index} className='bg-violet-100 hover:bg-violet-200'>
                            <td key={`${index}`} className='border-b border-white text-center'>{index + 1}</td>
                            {
                                fieldName.map((field: string) => (
                                    <td key={`${index}-${field}`} className='border-b border-white text-center'>{rowData[field]}</td>
                                ))
                            }
                            {
                                isActions &&
                                <td key={`${index}-Actions`} style={{ width: ActionsColumnWidth !== undefined ? ActionsColumnWidth : 'auto' }} className='border-b border-white flex justify-around'>
                                    {readFunc !== undefined && actionKey !== undefined && <button onClick={() => { readFunc(rowData[actionKey]) }} className='flex justify-center py-[5px] w-[100px] text-violet-800 bg-violet-50 border border-violet-400 rounded-md'><EyeOutlined /></button>}
                                    {updateFunc !== undefined && actionKey !== undefined && <button onClick={() => { updateFunc(rowData[actionKey]) }} className='flex justify-center py-[5px] w-[100px] text-gray-800 bg-gray-50 border border-gray-400 rounded-md'><EditOutlined /></button>}
                                    {deleteFunc !== undefined && actionKey !== undefined && <button onClick={() => { deleteFunc(rowData[actionKey]) }} className='flex justify-center py-[5px] w-[100px] text-red-800 bg-red-50 border border-red-400 rounded-md'><DeleteOutlined /></button>}
                                </td>
                            }
                        </tr>
                    ))}
            </tbody>
        </table>
    )
}