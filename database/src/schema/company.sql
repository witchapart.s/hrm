--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: company; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.company (
    "COMPANY_ID" character varying(100) NOT NULL,
    "COMPANY_CODE" character varying(50),
    "COMPANY_NAME_TH" character varying(200),
    "COMPANY_NAME_EN" character varying(200),
    "BUSINESS_TYPE" character varying(100),
    "REGISTERED_CAPITAL" numeric(20,0),
    "REGISTERED_DATE" date
);


ALTER TABLE public.company OWNER TO admin;

--
-- Name: company COMPANY_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT "COMPANY_pkey" PRIMARY KEY ("COMPANY_ID");


--
-- PostgreSQL database dump complete
--

