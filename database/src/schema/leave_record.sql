--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: leave_record; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.leave_record (
    "SK" character varying(250) NOT NULL,
    "EMP_ID" character varying(200),
    "LEAVE_DATE" date,
    "ACEPT_DATE" time without time zone,
    "LEAVE_TYPE" character varying(100),
    "APPLY_TYPE" character varying(50),
    "FILE_ATTACHED" character varying(100),
    "REMARKS" text,
    "ACEPTOR" character varying(200)
);


ALTER TABLE public.leave_record OWNER TO admin;

--
-- Name: leave_record LEAVE_RECORD_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.leave_record
    ADD CONSTRAINT "LEAVE_RECORD_pkey" PRIMARY KEY ("SK");


--
-- PostgreSQL database dump complete
--

