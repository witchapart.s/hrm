--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: work_and_ot_record; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.work_and_ot_record (
    "SK" character(250) NOT NULL,
    "EMP_ID" character varying(200),
    "DATE" date,
    "START_TIME" character varying(20),
    "END_TIME" character varying(20),
    "HOURS" numeric(5,0),
    "TYPE" character varying(20),
    "ACEPTOR" character varying(200)
);


ALTER TABLE public.work_and_ot_record OWNER TO admin;

--
-- Name: work_and_ot_record WORK_AND_OT_RECORD_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.work_and_ot_record
    ADD CONSTRAINT "WORK_AND_OT_RECORD_pkey" PRIMARY KEY ("SK");


--
-- PostgreSQL database dump complete
--

