--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: employee; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.employee (
    "EMP_ID" character varying(200) NOT NULL,
    "COMPANY_ID" character varying(100),
    "INTERNAL_EMP_ID" character varying(50),
    "TITLE_TH" character varying(20),
    "TITLE_EN" character varying(20),
    "FIRST_NAME_TH" character varying(150),
    "FIRST_NAME_EN" character varying(150),
    "LAST_NAME_TH" character varying(150),
    "LAST_NAME_EN" character varying(150),
    "INDENTIFICATION_NUMBER" character varying(30),
    "IDENTIFICATION_TYPE" character varying(100)
);


ALTER TABLE public.employee OWNER TO admin;

--
-- Name: employee EMPLOYEE_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT "EMPLOYEE_pkey" PRIMARY KEY ("EMP_ID");


--
-- PostgreSQL database dump complete
--

