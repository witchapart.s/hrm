--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.company ("COMPANY_ID", "COMPANY_CODE", "COMPANY_NAME_TH", "COMPANY_NAME_EN", "BUSINESS_TYPE", "REGISTERED_CAPITAL", "REGISTERED_DATE") FROM stdin;
01250382037	PSG	บริษัท ป้างซาจีส จำกัด	Pangsagis Co, ltd.	IT Consult	100000000	2000-10-02
10746039455	DP	บริษัท ดับเบิ้ลพี เอ็นเตอร์ไพรส์ จำกัด	Double P Enterprise Co, ltd.	IT Consult	28000000	1976-12-08
2495281172	KO	บริษัท ข้าวโอ้ต จำกัด	Kaooat Co, ltd.	Electonic	5000000	1945-11-05
\.


--
-- PostgreSQL database dump complete
--

