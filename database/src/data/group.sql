--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: group; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public."group" ("GROUP_ID", "COMPANY_ID", "GROUP_NAME") FROM stdin;
10746039455-HR-Manager	10746039455	HR Manager
10746039455-Programmer	10746039455	Programmer
01250382037-Programmer	01250382037	Programmer
01250382037-Financial	01250382037	Financial
02495281172-It-Support	2495281172	IT Support
\.


--
-- PostgreSQL database dump complete
--

