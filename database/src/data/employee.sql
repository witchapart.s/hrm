--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: employee; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.employee ("EMP_ID", "COMPANY_ID", "INTERNAL_EMP_ID", "TITLE_TH", "TITLE_EN", "FIRST_NAME_TH", "FIRST_NAME_EN", "LAST_NAME_TH", "LAST_NAME_EN", "INDENTIFICATION_NUMBER", "IDENTIFICATION_TYPE") FROM stdin;
10746039455-DP001	10746039455	DP001	นาย	Mr.	วิชญ์ภาส	Witchapart	สังข์เมือง	Sangmuang	1104300234938	ID card Number
10746039455-DP002	10746039455	DP002	นางสาว	Mr.	กิติเมษ	Kittimest	วงสกุลธรณ์	Wongsakultorn	1130459672023	ID card Number
01250382037-PSG001	01250382037	PSG001	นาย	Mr.	เตเต้	Taetae	จีวรบิน	G-wonbin	1240634935604	ID card number
01250382037-PSG002	01250382037	PSG002	นางสาว	Ms.	สิริวิมล	Siriwimon	Siriwimon	Manibor	2527645634586	ID card number
02495281172-KO001	02495281172	KO001	นาง	Mrs.	กาญจณา	Kanjana	Kanjana	Kitthura	3034135842048	ID card number
\.


--
-- PostgreSQL database dump complete
--

