--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: leave_record; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.leave_record ("SK", "EMP_ID", "LEAVE_DATE", "ACEPT_DATE", "LEAVE_TYPE", "APPLY_TYPE", "FILE_ATTACHED", "REMARKS", "ACEPTOR") FROM stdin;
\.


--
-- PostgreSQL database dump complete
--

