--
-- PostgreSQL database dump
--

-- Dumped from database version 11.16 (Debian 11.16-1.pgdg90+1)
-- Dumped by pg_dump version 11.16 (Debian 11.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: work_and_ot_record; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.work_and_ot_record ("SK", "EMP_ID", "DATE", "START_TIME", "END_TIME", "HOURS", "TYPE", "ACEPTOR") FROM stdin;
10746039455-DP001-2023-11-13                                                                                                                                                                                                                              	10746039455-DP001	2023-11-13	09.00	18.00	8	Work Day	\N
10746039455-DP001-2023-11-14                                                                                                                                                                                                                              	10746039455-DP001	2023-11-14	09.00	13.00	4	OT	10746039455-DP002
\.


--
-- PostgreSQL database dump complete
--

